﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Компонента для реализации эффекта взрыва
/// </summary>
public class Boom : MonoBehaviour
{
    /// <summary>
    /// Ссылка на компоненту звуков
    /// </summary>
    [SerializeField] private PlayerAudio playerAudio;
    /// <summary>
    /// Ссылка на систему частиц взрыва
    /// </summary>
    [SerializeField] private ParticleSystem ps;
    /// <summary>
    /// Ссылка на систему частиц брызг
    /// </summary>
    [SerializeField] private ParticleSystem psWater;
    /// <summary>
    /// Ссылка на эммитор системы частыц взрыва
    /// </summary>
    [SerializeField] private ParticleSystem.EmissionModule emission;

    /// <summary>
    /// Метод для начальной работы с системой частиц
    /// </summary>
    private void Start()
    {
        ps.Stop();
        emission = psWater.emission;
    }

    /// <summary>
    /// Метод для реализации системы взрыва
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T) && !ps.isPlaying)
        {
            playerAudio.StartBreakage();

            ps.Play();

            emission.rateOverTime = 0;
        }
        if (Input.GetKeyDown(KeyCode.Y))
        {
            ps.Stop();

            emission.rateOverTime = 50;

            playerAudio.StopRepair();
        }

    }
}
