﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Компонент-синглтон для управления игрой
/// </summary>
public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public GameStateController gameState = null;
    public ScenesManager scenes = null;
    public Countdown countdown = null;
    public ResourceManager resource = null;
    public LapManager lapManager = null;

    private void Awake()
    {
        if (instance == this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    private void OnDestroy()
    {
        instance = null;
    }

}
