﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
/// <summary>
/// Компонента для загрузки звуков игрока
/// </summary>
public class PlayerAudio : MonoBehaviour
{
    /// <summary>
    /// Компонент JetController игрока
    /// </summary>
    [SerializeField] private JetController jet;
    /// <summary>
    /// Максимальная громкость работы/старта двигателя
    /// </summary>
    [SerializeField] private float maxVolueEngine = 1f;
    /// <summary>
    /// Минимальная громкость работы/старта двигателя
    /// </summary>
    [SerializeField] private float minVolueEngine = 0.9f;
    /// <summary>
    /// Максимальная скорость игрока
    /// </summary>
    private float defaultMaxSpeed;
    /// <summary>
    /// Ссылка на аудио-миксер 
    /// </summary>
    private AudioMixer mixer;
    /// <summary>
    /// Ссылка на аудио-ресурс старта двигателся
    /// </summary>
    private AudioSource startEngine;
    /// <summary>
    /// Ссылка на аудио-ресурс работы двигателя
    /// </summary>
    private AudioSource runningEngine;
    /// <summary>
    /// Ссылка на аудио-ресурс удара
    /// </summary>
    private AudioSource hit;
    /// <summary>
    /// Ссылка на аудио-ресурс взрыва
    /// </summary>
    private AudioSource boom;
    /// <summary>
    /// Ссылка на аудио-ресурс починки
    /// </summary>
    private AudioSource repair;
    /// <summary>
    /// Ссылка на аудио-ресурс фальстарта
    /// </summary>
    private AudioSource falstart;
    /// <summary>
    /// Ссылка на аудио-ресурс бонуса
    /// </summary>
    private AudioSource bonus;

    /// <summary>
    /// Метод загрузки всех необходимых ресурсов и начальной работы с ними
    /// </summary>
    private void Start()
    {
        //Берем максимальную скорость
        defaultMaxSpeed = jet.MaxSpeed;
        //Берем ссылку на аудио-миксер
        mixer = Resources.Load<AudioMixer>("Audio/Game");

        //Берем ссылки на аудио-ресурсы

        runningEngine = gameObject.AddComponent<AudioSource>();

        startEngine = gameObject.AddComponent<AudioSource>();

        hit = gameObject.AddComponent<AudioSource>();

        boom = gameObject.AddComponent<AudioSource>();

        repair = gameObject.AddComponent<AudioSource>();

        falstart = gameObject.AddComponent<AudioSource>();

        bonus = gameObject.AddComponent<AudioSource>();

        //Загружаем аудио-клипы

        runningEngine.clip = Resources.Load<AudioClip>("Audio/RunningEngine");

        startEngine.clip = Resources.Load<AudioClip>("Audio/StartEngine");

        hit.clip = Resources.Load<AudioClip>("Audio/hit");

        boom.clip = Resources.Load<AudioClip>("Audio/Boom");

        repair.clip = Resources.Load<AudioClip>("Audio/Repair");

        falstart.clip = Resources.Load<AudioClip>("Audio/Falstart");

        bonus.clip = Resources.Load<AudioClip>("Audio/Bonus");

        //Включаем повтор необходимых аудио-клипов

        runningEngine.loop = true;

        startEngine.loop = false;

        hit.loop = false;

        boom.loop = false;

        repair.loop = true;

        falstart.loop = false;

        bonus.loop = false;

        //Подключаем аудио-группы для аудио-клипов

        runningEngine.outputAudioMixerGroup = mixer.FindMatchingGroups("Master")[2];

        startEngine.outputAudioMixerGroup = mixer.FindMatchingGroups("Master")[3];

        hit.outputAudioMixerGroup = mixer.FindMatchingGroups("Master")[4];

        boom.outputAudioMixerGroup = mixer.FindMatchingGroups("Master")[5];

        repair.outputAudioMixerGroup = mixer.FindMatchingGroups("Master")[6];

        falstart.outputAudioMixerGroup = mixer.FindMatchingGroups("Master")[7];

        bonus.outputAudioMixerGroup = mixer.FindMatchingGroups("Master")[8];
    }
    /// <summary>
    /// Метод для изменения громкости работы двигателя от скорости
    /// </summary>
    private void FixedUpdate()
    {
        float value = jet.Speed / defaultMaxSpeed;

        runningEngine.volume = Mathf.Lerp(minVolueEngine, maxVolueEngine, value);

        startEngine.volume = Mathf.Lerp(minVolueEngine, maxVolueEngine, value);

    }
    /// <summary>
    /// Метод для звукового сопровождения начала движения (после старта и поломки)
    /// </summary>
    public void StartMovement()
    {
        startEngine.Play();

        runningEngine.PlayDelayed(startEngine.clip.length);
    }

    /// <summary>
    /// Метод для звукового сопровождения удара об окружение
    /// </summary>
    public void PlayHit()
    {
        hit.Play();
    }

    /// <summary>
    /// Метод для звукового сопровождения поломки
    /// </summary>
    public void StartBreakage()
    {
        boom.Play();

        runningEngine.Stop();

        repair.Play();
    }

    /// <summary>
    /// Метод для звукового сопровождения конца починки
    /// </summary>
    public void StopRepair()
    {
        repair.Stop();

        StartMovement();
    }

    /// <summary>
    /// Метод для звукового сопровождения фальстарта
    /// </summary>
    public void PlayFalstart()
    {
        if (falstart.isPlaying == false)
        {
            runningEngine.Play();
        }        
    }

    /// <summary>
    /// Метод для звукового сопровождения подбора бонуса
    /// </summary>
    public void PlayBonus()
    {
        bonus.Play();
    }
}
