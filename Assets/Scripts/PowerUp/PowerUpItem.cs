﻿using UnityEngine;

/// <summary>
/// Класс объекта усиления
/// </summary>
[RequireComponent(typeof(Collider))]
public class PowerUpItem : MonoBehaviour
{
    public IPowerUp PowerUp { get { return powerUp; } }

    /// <summary>
    /// Типы усилений
    /// </summary>
    [System.Serializable]
    public enum Type { None, Acceleration, MineSet, Mine };

    /// <summary>
    /// Тип усиления
    /// </summary>
    public Type type;

    /// <summary>
    /// Максимальная скорость
    /// </summary>
    [Tooltip("Максимальная скорость")]
    [HideInInspector] public float maxSpeed = 0;

    /// <summary>
    /// Количество используемых предметов
    /// </summary>
    [Tooltip("Количество используемых предметов")]
    [HideInInspector] public int itemAmount = 0;

    /// <summary>
    /// Длительность эффекта
    /// </summary>
    [Tooltip("Длительность эффекта")]
    [HideInInspector] public float duration = 0;


    private IPowerUp powerUp;

    private void Awake()
    {
        gameObject.tag = "PowerUp";
        // в зависимости от типа усиления создаем объект соответствующего класса
        switch (type)
        {
            case Type.Acceleration:
                powerUp = new Acceleration(maxSpeed, duration);
                break;
            case Type.MineSet:
                powerUp = new MineSet(itemAmount);
                break;
            case Type.Mine:
                powerUp = new Mine(maxSpeed, duration);
                break;
            default:
                break;
        }
    }

}
