﻿using UnityEngine;

/// <summary>
/// Компонент конвертации скорости Rigidbody в "реальную" скорость
/// </summary>
public class SpeedConverter : MonoBehaviour
{
    /// <summary>
    /// Скорость переведенная в км/ч
    /// </summary>
    public float Speed { get; private set; } = 0;

    [Tooltip("Максимальная реальная скорость (для максимальной скорости JetController по умолчанию)")]
    /// <summary>
    /// Максимальная "реальная" скорость для максимальной скорости JetController по умолчанию
    /// </summary>
    [SerializeField] float maxRealSpeed = 100;

    [Tooltip("Ссылка на компонент JetController текущего игрока")]
    [SerializeField] JetController jet = null;
    private float defaultMaxSpeed = 0;

    private void Awake()
    {
        defaultMaxSpeed = jet.MaxSpeed;
    }

    void FixedUpdate()
    {
        float speedRate = jet.Speed / defaultMaxSpeed;
        if (speedRate == 0)
        {
            Speed = 0;
        }
        else
        {
            // обновляем реальную скорость
            float newValue = Mathf.FloorToInt(speedRate * maxRealSpeed);
            Speed = (int) Mathf.Lerp(Speed, newValue, Time.fixedDeltaTime * 10);
        }
    }
}
