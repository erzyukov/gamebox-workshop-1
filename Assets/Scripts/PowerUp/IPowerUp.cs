﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Интерфейс усилений
/// </summary>
public interface IPowerUp
{
    /// <summary>
    /// Цель на которую воздействует усиление
    /// </summary>
    GameObject Target { get; set; }

    /// <summary>
    /// Тип усиления
    /// </summary>
    PowerUpBehaviour Behaviour { get; set; }

    /// <summary>
    /// Усиление активированно
    /// </summary>
    bool Enabled { get; set; }

    /// <summary>
    /// Обработка поднятия усиления
    /// </summary>
    void PickUp(GameObject target);

    /// <summary>
    /// Обработка активации усиления
    /// </summary>
    void Enable();

    /// <summary>
    /// Обработка деактивации усиления
    /// </summary>
    void Disable();
}

/// <summary>
/// Типы усилений
/// Usable - используемый
/// Disposable - используемый один раз
/// Limited - используется ограниченное число
/// SelfActivating - активируется при поднятии и исчезает
/// Permanent - активируется при поднятии и активно все время
/// </summary>
public enum PowerUpBehaviour
{
    None, Usable, Disposable, Limited, SelfActivating, Permanent
}