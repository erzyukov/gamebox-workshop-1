﻿using UnityEngine;

/// <summary>
/// Компонент статуса игрока
/// </summary>
public class StatusController : MonoBehaviour
{
    public enum StatusTypes
    {
        None, Broken, Acceleration
    }

    /// <summary>
    /// Текущий статус игрока
    /// </summary>
    public StatusTypes Type { get; private set; } = StatusTypes.None;

    public delegate void ChangeStatusAction(StatusTypes type, float duration);
    public event ChangeStatusAction OnChangeStatus;

    /// <summary>
    /// Установка статуса игрока
    /// </summary>
    /// <param name="type">Тип статуса</param>
    /// <param name="duration">Длительность статуса</param>
    public void Set(StatusTypes type, float duration = 0)
    {
        this.Type = type;
        OnChangeStatus?.Invoke(type, duration);
    }

    /// <summary>
    /// Сброс текущего статуса
    /// </summary>
    public void Reset()
    {
        this.Type = StatusTypes.None;
        OnChangeStatus?.Invoke(StatusTypes.None, 0);
    }
}
