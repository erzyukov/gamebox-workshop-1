﻿using UnityEngine;

/// <summary>
/// Класс управляющий усилениями
/// </summary>
public class PowerUpContoller : MonoBehaviour
{
    public delegate void PickUpAction(PowerUpItem.Type type, int amount);
    /// <summary>
    /// Событие срабатывающее при подъеме усиления
    /// </summary>
    public event PickUpAction OnPickedUp;
    public delegate void PowerUpAction();
    /// <summary>
    /// Событие срабатывающее при использовании усиления
    /// </summary>
    public event PowerUpAction OnUse;

    private IPowerUp currentPowerUp = null;
    private PowerUpItem.Type currentPowertype = PowerUpItem.Type.None;
    private int currentAmount = 0;

    /// <summary>
    /// Использовать усиление (повторное использование отключает)
    /// </summary>
    public void Use()
    {
        if (currentPowerUp != null)
        {
            if (currentPowerUp.Behaviour == PowerUpBehaviour.Permanent)
            {
                return;
            }

            if (currentPowerUp.Enabled == false)
            {
                currentPowerUp.Enable();
                if (currentPowerUp.Behaviour == PowerUpBehaviour.Disposable)
                {
                    currentPowerUp = null;
                }
                currentAmount--;
                OnUse?.Invoke();

                if (currentPowerUp != null && currentPowerUp.Behaviour == PowerUpBehaviour.Limited && currentAmount <= 0)
                {
                    currentPowerUp = null;
                }
            }
            else
            {
                currentPowerUp.Disable();
            }
            
        }
    }

    /// <summary>
    /// Подобрать усиление
    /// </summary>
    /// <param name="powerUpObject">объект усиления</param>
    private void PickUp(GameObject powerUpObject)
    {
        PowerUpItem powerUpItem = powerUpObject.GetComponent<PowerUpItem>();
        IPowerUp powerUp = powerUpItem.PowerUp;

        // поднимаем усиление
        powerUp.PickUp(gameObject);

        // если уселитель не самоактивирующийся, то заменяем им имеющийся
        if (powerUp.Behaviour != PowerUpBehaviour.SelfActivating)
        {
            if (powerUp.Behaviour == PowerUpBehaviour.Limited)
            {
                currentAmount = (currentPowertype == powerUpItem.type)? currentAmount + powerUpItem.itemAmount: powerUpItem.itemAmount;
            }
            else if (powerUp.Behaviour == PowerUpBehaviour.Disposable)
            {
                currentAmount = 1;
            }
            OnPickedUp?.Invoke(powerUpItem.type, currentAmount);

            if (currentPowerUp != null)
            {
                currentPowerUp.Disable();
            }
            currentPowerUp = powerUp;

            currentPowertype = powerUpItem.type;
        }

        Destroy(powerUpObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PowerUp"))
        {
            PickUp(other.gameObject);
        }
    }

}
