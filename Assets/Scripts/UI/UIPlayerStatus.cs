﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Компонент пользовательского интерфейска отображающего активный статус
/// </summary>
public class UIPlayerStatus : MonoBehaviour
{
    [Tooltip("Аниматор иконки статуса")]
    [SerializeField] private Animator animator = null;

    [Tooltip("Иконка статуса")]
    [SerializeField] private Image icon = null;
    [Tooltip("Бакграунд прогрессбара статуса")]
    [SerializeField] private Image progressBG = null;
    [Tooltip("Значение прогрессбара статуса")]
    [SerializeField] private Image progressValue = null;

    [Tooltip("Номер игрока")]
    [SerializeField] private PlayerTypes type = default;

    private StatusController player = null;

    private Coroutine statusCoroutine = null;

    private Sprite BlackProgressBar = null;
    private Sprite BlueProgressBar = null;
    private Sprite GreenProgressBar = null;
    private Sprite OrangeProgressBar = null;
    private Sprite RedProgressBar = null;
    private Sprite YellowProgressBar = null;

    private void Awake()
    {
        // загружаем спрайты прогрессбаров
        Sprite[] bg = Resources.LoadAll<Sprite>("UI/ProgressBars");
        if (bg.Length > 5)
        {
            BlackProgressBar = bg[0];
            BlueProgressBar = bg[1];
            GreenProgressBar = bg[2];
            OrangeProgressBar = bg[3];
            RedProgressBar = bg[4];
            YellowProgressBar = bg[5];
        }

        ResetStatus();
    }

    private void Start()
    {
        player = GameManager.instance.resource.GetPlayer(type).GetComponent<StatusController>();
        if (player != null)
        {
            player.OnChangeStatus += OnChangeStatus;
        }
    }

    /// <summary>
    /// Обработка смены статуса
    /// </summary>
    /// <param name="type">Тип статуса</param>
    /// <param name="duration">Длительность статуса</param>
    private void OnChangeStatus(StatusController.StatusTypes type, float duration)
    {
        if (statusCoroutine != null)
        {
            StopCoroutine(statusCoroutine);
        }
        switch (type)
        {
            case StatusController.StatusTypes.None:
                ResetStatus();
                break;
            case StatusController.StatusTypes.Broken:
                progressBG.sprite = RedProgressBar;
                progressValue.sprite = BlueProgressBar;
                SetStatus(type, duration);
                break;
            case StatusController.StatusTypes.Acceleration:
                progressBG.sprite = GreenProgressBar;
                progressValue.sprite = OrangeProgressBar;
                SetStatus(type, duration);
                break;
        }
    }

    /// <summary>
    /// Устанавливает статус и делает его активным
    /// </summary>
    /// <param name="type">Тип статуса</param>
    /// <param name="duration">Длительность статуса</param>
    private void SetStatus(StatusController.StatusTypes type, float duration)
    {
        icon.sprite = Resources.Load<Sprite>("UI/Icons/" + type);
        icon.enabled = true;
        animator.SetBool("Active", true);
        if (duration > 0)
        {
            progressBG.enabled = true;
            progressValue.enabled = true;
            statusCoroutine = StartCoroutine(Progress(duration));
        }
    }

    /// <summary>
    /// Изменяет показатель прогрессбара в зависимости от прошедшего времени
    /// </summary>
    /// <param name="duration">Длительность</param>
    /// <returns></returns>
    private IEnumerator Progress(float duration)
    {
        float timePassed = 0;
        while (timePassed < duration)
        {
            if (GameManager.instance.gameState.IsActionAllow())
            {
                timePassed += Time.fixedUnscaledDeltaTime;
                progressValue.fillAmount = Mathf.Clamp01(timePassed / duration);
            }
            yield return new WaitForFixedUpdate();
        }
    }

    /// <summary>
    /// Сбрасывает текущий статус
    /// </summary>
    private void ResetStatus()
    {
        animator.SetBool("Active", false);
        icon.enabled = false;
        progressBG.enabled = false;
        progressValue.enabled = false;
    }

    private void OnDestroy()
    {
        if (player != null)
        {
            player.OnChangeStatus -= OnChangeStatus;
        }
    }
}