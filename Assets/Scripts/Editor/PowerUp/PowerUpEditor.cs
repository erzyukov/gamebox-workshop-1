﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PowerUpItem))]
public class PowerUpEditor : Editor
{
    SerializedProperty maxSpeed;
    SerializedProperty itemAmount;
    SerializedProperty duration;

    protected virtual void OnEnable()
    {
        maxSpeed = this.serializedObject.FindProperty("maxSpeed");
        itemAmount = this.serializedObject.FindProperty("itemAmount");
        duration = this.serializedObject.FindProperty("duration");

        PowerUpItem item = (PowerUpItem)target;
        GameObject obj = item.gameObject;
        obj.tag = "PowerUp";
        obj.GetComponent<Collider>().isTrigger = true;
    }

    public override void OnInspectorGUI()
    {
        this.serializedObject.Update();

        PowerUpItem item = (PowerUpItem)target;

        EditorGUI.BeginChangeCheck();

        base.OnInspectorGUI();

        switch (item.type)
        {
            case PowerUpItem.Type.Acceleration:
                EditorGUILayout.PropertyField(maxSpeed);
                EditorGUILayout.PropertyField(duration);
                EditorGUILayout.PropertyField(itemAmount);
                break;
            case PowerUpItem.Type.Mine:
                EditorGUILayout.PropertyField(maxSpeed);
                EditorGUILayout.PropertyField(duration);
                break;
            case PowerUpItem.Type.MineSet:
                EditorGUILayout.PropertyField(itemAmount);
                break;
        }

        this.serializedObject.ApplyModifiedProperties();
    }
}