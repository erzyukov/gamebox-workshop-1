﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Компонент пользовательского интерфейска отображающего неверное направление движения
/// </summary>
public class UIPlayerWrongWay : MonoBehaviour
{
    [Tooltip("Аниматор предупреждения")]
    [SerializeField] private Animator animator = null;

    [Tooltip("Объект с содержимым предупреждения")]
    [SerializeField] private GameObject plate = null;

    [Tooltip("Время мерцания индикатора")]
    [SerializeField] private float indicatorShowTime = 3;

    [Tooltip("Номер игрока")]
    [SerializeField] private PlayerTypes type = default;

    private PlayerLapHandler player;

    private Coroutine warningCoroutine;

    private void Start()
    {
        player = GameManager.instance.resource.GetPlayer(type).GetComponent<PlayerLapHandler>();
        plate.SetActive(false);
        if (player != null)
        {
            player.OnWrongWay += OnWrongWay;
            player.OnRightWay += OnRightWay;
        }
    }

    /// <summary>
    /// Обработка неверного движения игрока
    /// </summary>
    private void OnWrongWay()
    {
        // если игрок плывет не туда - запускаем мерцание индикатора
        if (warningCoroutine != null)
        {
            StopCoroutine(warningCoroutine);
        }
        plate.SetActive(true);
        animator.SetBool("Active", true);
        warningCoroutine = StartCoroutine(OnWrongWayCoroutine());
    }

    /// <summary>
    /// Обработка верного движения игрока
    /// </summary>
    private void OnRightWay()
    {
        // если игрок плывет в нужном направлении - убираем индикатор если он был
        if (warningCoroutine != null)
        {
            StopCoroutine(warningCoroutine);
            warningCoroutine = null;
        }
        plate.SetActive(false);
        animator.SetBool("Active", false);
    }

    private IEnumerator OnWrongWayCoroutine()
    {
        yield return new WaitForSeconds(indicatorShowTime);
        OnRightWay();
    }

    private void OnDestroy()
    {
        if (player != null)
        {
            player.OnWrongWay -= OnWrongWay;
            player.OnRightWay -= OnRightWay;
        }
    }
}
