﻿using UnityEngine;

/// <summary>
/// Усиление. Повреждает игрока.
/// </summary>
public class Mine : IPowerUp
{
    public GameObject Target { get; set; }

    public PowerUpBehaviour Behaviour { get; set; }

    public bool Enabled { get; set; }

    private BreakdownController targetBC;
    private float maxSpeed;
    private float duration;

    public Mine(float maxSpeed, float duration)
    {
        Behaviour = PowerUpBehaviour.SelfActivating;
        Enabled = false;
        this.maxSpeed = maxSpeed;
        this.duration = duration;
    }

    public void PickUp(GameObject target)
    {
        Target = target;
        Enabled = true;
        targetBC = target.GetComponent<BreakdownController>();
        targetBC.Break(duration, maxSpeed);
    }

    public void Disable()
    {
    }

    public void Enable()
    {
    }

}
