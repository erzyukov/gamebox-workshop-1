﻿using UnityEngine;
using System;

/// <summary>
/// Компонент контрольной точки
/// </summary>
[RequireComponent(typeof(Collider))]
public class CheckPoint : MonoBehaviour
{
    /// <summary>
    /// Возможные типы контрольной точки
    /// </summary>
    public enum CheckPointType { Point, Start, Finish };

    /// <summary>
    /// Тип события срабатывающее при прересечении контрольной точки
    /// В событие передаются номер контрольной точки, тип контрольной точки и идентификатор объекта на который сработал триггер
    /// </summary>
    /// <param name="indexNumber">Порядковый номер контрольной точки</param>
    /// <param name="type">Тип контрольной точки</param>
    /// <param name="gameObjectID">Идентификатор объекта, который вызвал срабатывание триггера</param>
    public delegate void PointCrossAction(int indexNumber, CheckPointType type, int gameObjectID);
    /// <summary>
    /// Событие срабатывающее при пересечении контрольной точки
    /// </summary>
    public event PointCrossAction OnPointCross;

    [Tooltip("Порядковый номер")]
    [SerializeField] private int indexNumber = 0;

    [Tooltip("Тип контрольной точки")]
    [SerializeField] private CheckPointType type = CheckPointType.Point;

    private void Awake()
    {
        // объект контрольной точки обязательно должен быть статичным
        if (!gameObject.isStatic)
        {
            throw new Exception("Объект контрольной точки должен быть статичный! (Object name: \"" + gameObject.name + "\")");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        OnPointCross?.Invoke(indexNumber, type, other.gameObject.GetInstanceID());
    }

}