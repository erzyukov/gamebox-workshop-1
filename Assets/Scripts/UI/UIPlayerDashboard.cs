﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Компонент пользовательского интерфейска отображающего параметры игрока
/// </summary>
public class UIPlayerDashboard : MonoBehaviour
{
    [Tooltip("Имя игрока")]
    [SerializeField] private Text playerName = null;
    [Tooltip("Скорость игрока")]
    [SerializeField] private Text speed = null;
    [Tooltip("Текущий круг игрока")]
    [SerializeField] private Text lap = null;

    [Tooltip("Номер игрока")]
    [SerializeField] private PlayerTypes type = default;

    private SpeedConverter playerSpeedConverter = null;
    private PlayerLapHandler playerLapHandler = null;
    private int maxLapNumber = 0;

    private void Start()
    {
        playerSpeedConverter = GameManager.instance.resource.GetPlayer(type).GetComponent<SpeedConverter>();
        playerLapHandler = GameManager.instance.resource.GetPlayer(type).GetComponent<PlayerLapHandler>();

        if (playerLapHandler != null)
        {
            playerLapHandler.OnLapPassed += OnLapPassed;
        }

        maxLapNumber = GameManager.instance.lapManager.MaxLapNumber;
        OnLapPassed();

        // проставляем имя
        playerName.text = playerLapHandler.gameObject.name;
    }
    private void FixedUpdate()
    {
        // проставляем скорость
        speed.text = playerSpeedConverter.Speed.ToString();
    }

    /// <summary>
    /// Обработка события прохождения круга
    /// </summary>
    /// <param name="number"></param>
    /// <param name="player"></param>
    private void OnLapPassed()
    {
        if (playerLapHandler.LapNumber <= maxLapNumber)
        {
            lap.text = playerLapHandler.LapNumber + " / " + maxLapNumber;
        }
    }
}
