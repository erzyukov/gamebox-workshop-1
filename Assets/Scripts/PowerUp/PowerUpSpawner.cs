﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Компонент спауна усилений
/// </summary>
public class PowerUpSpawner : MonoBehaviour
{
    /// <summary>
    /// Места для спауна усилений, усиления будут зеркалиться относительно Vector3.zero
    /// </summary>
    [Tooltip("Места для спауна усилений, усиления будут зеркалиться относительно Vector3.zero")]
    [SerializeField] private BoxCollider[] spawnPlaces = default;

    /// <summary>
    /// Минимальное время спауна
    /// </summary>
    [Tooltip("Минимальное время спауна")]
    [SerializeField] private float minSpawnTime = 2f;
    /// <summary>
    /// Максимальное время спауна
    /// </summary>
    [Tooltip("Максимальное время спауна")]
    [SerializeField] private float maxSpawnTime = 5f;

    /// <summary>
    /// Время, после которого усиление пропадет
    /// </summary>
    [Tooltip("Время, после которого усиление пропадет")]
    [SerializeField] private float lifeTime = 5f;

    /// <summary>
    /// Массив названий ресурсов с усилениями
    /// </summary>
    [Tooltip("Массив названий ресурсов с усилениями (лежат в папке /Resources/PowerUp/)")]
    [SerializeField] private string[] powerUpResourceNames = default;

    private GameObject[] powerUpPrefabs;

    private void Awake()
    {
        // собираем все префабы указанных усилений
        int count = powerUpResourceNames.Length;
        powerUpPrefabs = new GameObject[count];
        for (int i = 0; i < count; i++){
            powerUpPrefabs[i] = Resources.Load<GameObject>("PowerUp/" + powerUpResourceNames[i]);
        }
    }

    private void Start()
    {
        // запускаем спаун
        StartCoroutine(SpawnCoroutine());
    }

    /// <summary>
    /// Спаунит усиление в случайной точке и дублирует его относительно Vector3.zero
    /// </summary>
    private void SpawnPowerUp()
    {
        GameObject prefab = powerUpPrefabs[Random.Range(0, powerUpPrefabs.Length)];
        Vector3 point = GetRandomPoint();
        Vector3 mirrorPoint = GetMirrorPoint(point);
        GameObject obj1 = Instantiate<GameObject>(prefab, point, Quaternion.identity);
        GameObject obj2 = Instantiate<GameObject>(prefab, mirrorPoint, Quaternion.identity);
        StartCoroutine(DestroyPowerUpCoroutine(lifeTime, obj1));
        StartCoroutine(DestroyPowerUpCoroutine(lifeTime, obj2));
    }

    /// <summary>
    /// Возвращает зеркальную точку
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    private Vector3 GetMirrorPoint(Vector3 point)
    {
        return new Vector3(-point.x, point.y, -point.z);
    }

    /// <summary>
    /// Возвращает случайную точку из случайной области
    /// </summary>
    /// <returns>Случайная точка</returns>
    private Vector3 GetRandomPoint()
    {
        BoxCollider spawnPlace = spawnPlaces[Random.Range(0, spawnPlaces.Length)];
        Vector3 center = spawnPlace.bounds.center;
        Vector3 size = spawnPlace.bounds.size;
        float x = Random.Range(center.x - size.x / 2, center.x + size.x / 2);
        float y = size.y;
        float z = Random.Range(center.z - size.z / 2, center.z + size.z / 2);

        return new Vector3(x, y, z);
    }

    private IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            float duration = Random.Range(minSpawnTime, maxSpawnTime);
            float timePassed = 0;
            while (timePassed < duration)
            {
                if (GameManager.instance.gameState.IsActionAllow())
                {
                    timePassed += Time.fixedUnscaledDeltaTime;
                }
                yield return new WaitForFixedUpdate();
            }
            SpawnPowerUp();
        }
    }

    private IEnumerator DestroyPowerUpCoroutine(float duration, GameObject obj)
    {
        float timePassed = 0;
        while (timePassed < duration)
        {
            if (GameManager.instance.gameState.IsActionAllow())
            {
                timePassed += Time.fixedUnscaledDeltaTime;
            }
            yield return new WaitForFixedUpdate();
        }
        if (obj != null)
        {
            Destroy(obj);
        }
    }

}
