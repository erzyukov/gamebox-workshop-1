﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Компонент управления окном победы
/// </summary>
public class VictoryWindow : Window
{
    /// <summary>
    /// Имя победителя
    /// </summary>
    [Tooltip("Ссылка на текстовый компонент который отображает имя победителя")]
    [SerializeField] protected Text winnerName;

    private ConfirmWindow confirm = null;

    /// <summary>
    /// Устанавливает окно подтверждения
    /// </summary>
    /// <param name="window">Окно подтверждения</param>
    public void SetConfirmWindow(ConfirmWindow window)
    {
        confirm = window;
    }

    /// <summary>
    /// Открывает окно победителя и включает паузу
    /// </summary>
    public override void Open()
    {
        base.Open();
        GameManager.instance.gameState.PauseGame();
    }

    /// <summary>
    /// Проставляет имя победителя
    /// </summary>
    /// <param name="name">Имя победителя</param>
    public void SetWinnerName(string name)
    {
        winnerName.text = name;
    }

    /// <summary>
    /// Обработка нажатия кнопки повторить игру
    /// </summary>
    public void RepeatGameButtonHandler()
    {
        GameManager.instance.scenes.RestatrLevel();
    }

    /// <summary>
    /// Обработка нажатия кнопки главное меню
    /// </summary>
    public void MainMenuButtonHandler()
    {
        GameManager.instance.scenes.LoadMainMenu();
    }

    /// <summary>
    /// Обработка нажатия кнопки выход
    /// </summary>
    public void QuitButtonHandler()
    {
        // открываем окно подтверждения и закрываем базовое окно без всяких действий
        confirm.Open();
        base.Close();
        confirm.SetYesAction(() => {
            // на согласие выходим из игры
            GameManager.instance.scenes.QuitGame();
        });
        confirm.SetNoAction(() => {
            // на отказ закрываем окно подтверждения и возвращаем отображение базового окна
            confirm.Close();
            base.Open();
        });
    }

}
