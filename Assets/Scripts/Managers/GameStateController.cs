﻿using UnityEngine;

/// <summary>
/// Компонент управляющий состоянием игрового процесса
/// </summary>
public class GameStateController : MonoBehaviour
{
    /// <summary>
    /// Текущее состояние игрового процесса
    /// </summary>
    public enum StateType { None, Countdown, Game, Pause, GameOver, Quit};
    public StateType State { get { return state; } }

    private StateType state;

    private StateType previousState;

    public delegate void GameStateAction(StateType type);
    /// <summary>
    /// Событие срабатывающее при изменении состоянии игры
    /// </summary>
    public event GameStateAction OnGameStateChange;

    /// <summary>
    /// Перейти в состояние Пауза (StateType.Pause)
    /// </summary>
    public void PauseGame()
    {
        if (state != StateType.Pause)
        {
            SetPreviousState();
            state = StateType.Pause;
            OnGameStateChange?.Invoke(state);
            Time.timeScale = 0;
        }
    }

    /// <summary>
    /// Продолжить игру после паузы
    /// </summary>
    public void ResumeGame()
    {
        Time.timeScale = 1;
        if (GameManager.instance.countdown != null)
        {
            state = StateType.Countdown;
            GameManager.instance.countdown.StartCountdown(() => {
                state = previousState;
                OnGameStateChange?.Invoke(state);
            });
        }
        else
        {
            state = previousState;
            OnGameStateChange?.Invoke(state);
        }
    }

    /// <summary>
    /// Проверяет разрешина ли пауза
    /// </summary>
    /// <returns></returns>
    public bool IsPauseAllow()
    {
        if (state == StateType.GameOver || state == StateType.None || state == StateType.Countdown)
        {
            return false;
        }

        return true;
    }

    /// <summary>
    /// Проверяет разрешино ли движение
    /// </summary>
    /// <returns></returns>
    public bool IsActionAllow()
    {
        if (state == StateType.Game)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Начинает игру
    /// </summary>
    public void StartGame()
    {
        Time.timeScale = 1;
        if (GameManager.instance.countdown != null)
        {
            state = StateType.Countdown;
            GameManager.instance.countdown.StartCountdown(() => {
                state = StateType.Game;
                OnGameStateChange?.Invoke(state);
            });
        }
        else
        {
            state = StateType.Game;
            OnGameStateChange?.Invoke(state);
        }
    }

    /// <summary>
    /// Закончить игру
    /// </summary>
    public void FinishGame(string winerName)
    {
        GameManager.instance.resource.VictoryWindow.SetWinnerName(winerName);
        GameManager.instance.resource.VictoryWindow.Open();
        state = StateType.GameOver;
        OnGameStateChange?.Invoke(state);
    }

    /// <summary>
    /// Устанавливает предыдущее состояние
    /// Пока может устанавливаться только StateType.Game
    /// Остальные пока не нужны. Функция для удобства в будущем.
    /// </summary>
    private void SetPreviousState()
    {
        switch (state)
        {
            case StateType.Game:
                previousState = state;
                break;
            default:
                previousState = StateType.Game;
                break;
        }
    }


}
