﻿using UnityEngine;

/// <summary>
/// Усиление. Получает мины, которые можно будет сбрасывать.
/// </summary>
public class MineSet : IPowerUp
{
    public GameObject Target { get; set; }

    public PowerUpBehaviour Behaviour { get; set; }

    public bool Enabled { get; set; }

    private float itemAmount;
    private GameObject minePrefab;

    public MineSet(int itemAmount)
    {
        Behaviour = PowerUpBehaviour.Limited;
        Enabled = false;
        this.itemAmount = itemAmount;
        minePrefab = Resources.Load<GameObject>("PowerUp/Mine");
    }

    void IPowerUp.PickUp(GameObject target)
    {
        Target = target;
    }

    void IPowerUp.Enable()
    {
        Vector3 spawnPosition = Target.transform.position - Target.transform.forward * 10f;
        GameObject.Instantiate(minePrefab, spawnPosition, Quaternion.identity);
    }

    void IPowerUp.Disable()
    {
    }

}
