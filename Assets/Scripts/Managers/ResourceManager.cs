﻿using UnityEngine;

/// <summary>
/// Компонент управления ресурсами игры
/// </summary>
public class ResourceManager : MonoBehaviour
{
    /// <summary>
    /// Объект первого игрока
    /// </summary>
    public GameObject PlayerOne { get; private set; }
    /// <summary>
    /// Объект второго игрока
    /// </summary>
    public GameObject PlayerTwo { get; private set; }

    /// <summary>
    /// Компонент взаимодействия с пользовательским интерфейсом
    /// </summary>
    public UIActions UIActions { get; private set; }

    /// <summary>
    /// Окно информирования о победе
    /// </summary>
    public VictoryWindow VictoryWindow { get; private set; }

    /// <summary>
    /// Тип сцены на которой находится Менеджер игры
    /// </summary>
    public SceneType Type { get { return type; } }

    public enum SceneType { Menu, Game }

    [Tooltip("Имя первого игрока")]
    [SerializeField] private string PlayerOneName = "PlayerOne";
    [Tooltip("Имя второго игрока")]
    [SerializeField] private string PlayerTwoName = "PlayerTwo";

    [Space]

    [Tooltip("Тип сцены")]
    [SerializeField] private SceneType type = SceneType.Menu;
    [Tooltip("Имя трассы, указывается если сцена игровая")]
    [SerializeField] private string trackName = "";

    private GameObject currentTrack;
    private TrackController currentTrackController;

    private void Awake()
    {
        switch (type)
        {
            case SceneType.Menu:
                InitMenu();
                break;
            case SceneType.Game:
                InitGameTrack(trackName);
                InitGameUI();
                break;
        }
    }

    public GameObject GetPlayer(PlayerTypes type)
    {
        switch (type)
        {
            case PlayerTypes.One:
                return PlayerOne;
            case PlayerTypes.Two:
                return PlayerTwo;
            default:
                return default;
        }
    }

    private void InitMenu()
    {

    }

    /// <summary>
    /// Инициализация игрового интерфейса
    /// </summary>
    private void InitGameUI()
    {
        GameObject EventSystem = Instantiate(Resources.Load<GameObject>("UI/EventSystem"));

        GameObject playerInterface = Instantiate(Resources.Load<GameObject>("UI/PlayerInterface"), EventSystem.transform);
        UIActions = playerInterface.GetComponent<UIActions>();
        GameObject confirm = Instantiate(Resources.Load<GameObject>("UI/Window/Confirm"), EventSystem.transform);
        ConfirmWindow confirmWindow = confirm.GetComponent<ConfirmWindow>();

        GameObject pause = Instantiate(Resources.Load<GameObject>("UI/Window/Pause"), EventSystem.transform);
        pause.GetComponent<PauseWindow>().SetConfirmWindow(confirmWindow);
        GameObject victory = Instantiate(Resources.Load<GameObject>("UI/Window/Victory"), EventSystem.transform);
        VictoryWindow = victory.GetComponent<VictoryWindow>();
        VictoryWindow.SetConfirmWindow(confirmWindow);
    }

    /// <summary>
    /// Инициализация иговых объектов. Запускает трассу по имени и устанавливает игроков на нее.
    /// </summary>
    /// <param name="name">Имя трассы</param>
    private void InitGameTrack(string name)
    {
        currentTrack = Instantiate(Resources.Load<GameObject>("Track/" + name));
        currentTrackController = currentTrack.GetComponent<TrackController>();

        PlayerOne = Instantiate(Resources.Load<GameObject>("Player/One"), currentTrackController.StartPointOne.position, currentTrackController.StartPointOne.rotation);
        PlayerTwo = Instantiate(Resources.Load<GameObject>("Player/Two"), currentTrackController.StartPointTwo.position, currentTrackController.StartPointTwo.rotation);

        PlayerOne.name = PlayerOneName;
        PlayerTwo.name = PlayerTwoName;

        currentTrackController.InitPlayerOneLap(PlayerOne.GetComponent<PlayerLapHandler>());
        currentTrackController.InitPlayerTwoLap(PlayerTwo.GetComponent<PlayerLapHandler>());
    }

}
