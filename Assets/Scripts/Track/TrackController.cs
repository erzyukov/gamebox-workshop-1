﻿using UnityEngine;

/// <summary>
/// Компонент управления трассой, для инициализации всех объектов трассы
/// </summary>
public class TrackController : MonoBehaviour
{
    /// <summary>
    /// Стартовая точка для первого игрока
    /// </summary>
    public Transform StartPointOne { get { return startPointOne; } }
    /// <summary>
    /// Стартовая точка для второго игрока
    /// </summary>
    public Transform StartPointTwo { get { return startPointTwo; } }

    [Tooltip("Ссылка на круг для первого игрока")]
    [SerializeField] private PassingLapController playerOneLap = null;
    [Tooltip("Ссылка на круг для второго игрока")]
    [SerializeField] private PassingLapController playerTwoLap = null;

    [Tooltip("Ссылка на стартовую точку для первого игрока")]
    [SerializeField] private Transform startPointOne = null;
    [Tooltip("Ссылка на стартовую точку для второго игрока")]
    [SerializeField] private Transform startPointTwo = null;

    private void Awake()
    {
        // на старте деактивируем, и активируем при инициализации игрока
        playerOneLap.gameObject.SetActive(false);
        playerTwoLap.gameObject.SetActive(false);
    }

    /// <summary>
    /// Инициализирует трассу для первого игрока
    /// </summary>
    /// <param name="playerLapHandler">Первый игрок</param>
    public void InitPlayerOneLap(PlayerLapHandler playerLapHandler)
    {
        playerOneLap.gameObject.SetActive(true);
        playerOneLap.InitLap(playerLapHandler);
    }

    /// <summary>
    /// Инициализирует трассу для второго игрока
    /// </summary>
    /// <param name="playerLapHandler">Второй игрок</param>
    public void InitPlayerTwoLap(PlayerLapHandler playerLapHandler)
    {
        playerTwoLap.gameObject.SetActive(true);
        playerTwoLap.InitLap(playerLapHandler);
    }

}
