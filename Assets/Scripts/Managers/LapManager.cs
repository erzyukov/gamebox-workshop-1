﻿using UnityEngine;

/// <summary>
/// Компонента определяющая победителя по количеству пройденных кругов
/// </summary>
public class LapManager : MonoBehaviour
{
    public int MaxLapNumber { get { return maxLapNumber; } }

    [Tooltip("Количество кругов необходимое для победы")]
    /// <summary>
    /// Количество кругов необходимое для победы
    /// </summary>
    [SerializeField] private int maxLapNumber = 10;

    private PlayerLapHandler playerOne = null;
    private PlayerLapHandler playerTwo = null;

    private void Start()
    {
        if (GameManager.instance.resource.Type != ResourceManager.SceneType.Game)
        {
            return;
        }

        playerOne = GameManager.instance.resource.PlayerOne.GetComponent<PlayerLapHandler>();
        playerTwo = GameManager.instance.resource.PlayerTwo.GetComponent<PlayerLapHandler>();

        if (playerOne != null)
        {
            playerOne.OnPlayerWin += OnPlayerWin;
        }
        if (playerTwo != null)
        {
            playerTwo.OnPlayerWin += OnPlayerWin;
        }
    }

    /// <summary>
    /// Вызывается при победе одного из игроков
    /// </summary>
    private void OnPlayerWin(string name)
    {
        GameManager.instance.gameState.FinishGame(name);
    }

    private void OnDestroy()
    {
        if (playerOne != null)
        {
            playerOne.OnPlayerWin -= OnPlayerWin;
        }
        if (playerTwo != null)
        {
            playerTwo.OnPlayerWin -= OnPlayerWin;
        }
    }

}
