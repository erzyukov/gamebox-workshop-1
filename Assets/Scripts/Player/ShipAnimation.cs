﻿using UnityEngine;
/// <summary>
/// Компонент анимации движения игрока по волнам
/// </summary>
public class ShipAnimation : MonoBehaviour
{   
    /// <summary>
    /// Разница между начальной позицией игрока и максимальной высотой
    /// </summary>
    [Tooltip("Максимальное расстояние на которое поднимится лодка при максимальной скорости")]
    [SerializeField] private float deltaHeight = 0;
    /// <summary>
    /// Разница между начальным уголом наклона игрока и максимальным
    /// </summary>
    [Tooltip("Максимальный угол подъема носа лодки при максимальной скорости")]
    [SerializeField] private float deltaRotation = 0;

    /// <summary>
    /// Ссылка на игровой объект с меш-рендером
    /// </summary>
    [Tooltip("Ссылка на объект модели игрока")]
    [SerializeField] private GameObject ship = default;
    /// <summary>
    /// Ссылка на JetController игрока
    /// </summary>
    [Tooltip("Ссылка на компонент JetController игрока")]
    [SerializeField] private JetController jet = null;

    /// <summary>
    /// Стартовая позиция игрока
    /// </summary>
    private Vector3 startPos;
    /// <summary>
    /// Стартовый наклон игрока
    /// </summary>
    private Quaternion startRot;
    
    private void Start()
    {
        //Берем начальную позицию игрока
        startPos = ship.transform.localPosition;
        //Берем начальный угол наклона игрока
        startRot = ship.transform.localRotation;
    }

    private void Update()
    {
        //Находим отношение настоящей скорости к максимаьной
        float speedRate = (jet.MaxSpeed > 0)? jet.Speed / jet.MaxSpeed: 0;
        //Перемещаем игрока
        ship.transform.localPosition = Vector3.Lerp(startPos, new Vector3(startPos.x, startPos.y + deltaHeight, startPos.z), speedRate);
        //Наклоняем игрока
        ship.transform.localRotation = Quaternion.Lerp(startRot, Quaternion.Euler(startRot.x + deltaRotation, startRot.y, startRot.z), speedRate);
    }
}
