﻿using UnityEngine;

/// <summary>
/// Настройка системы частиц в зависимости от состояния игрока
/// </summary>
public class WaterPlumeController : MonoBehaviour
{
    /// <summary>
    /// Коэффициент увеличения количества частиц
    /// </summary>
    [Tooltip("Множитель количества частиц")]
    [SerializeField] private float particleMultiplier = 0.5f;
    /// <summary>
    /// Ссылка на JetController игрока
    /// </summary>
    [Tooltip("Ссылка на компонент JetController игрока")]
    [SerializeField] private JetController jet = null;
    /// <summary>
    /// Ссылка на систему частиц игрока
    /// </summary>
    [Tooltip("Ссылка на компонент ParticleSystem водного шлейфа игрока")]
    [SerializeField] private ParticleSystem particles = null;
    
    /// <summary>
    /// Ссылка на модуль emission системы частиц игрока
    /// </summary>
    private ParticleSystem.EmissionModule emission = default;

    private void Start()
    {       
        emission = particles.emission;
        emission.rateOverDistance = 0f;
    }

    private void Update()
    {
        //Увеличиваем количество частиц в зависимости от скорости игрока и коэффициента частиц
        emission.rateOverDistance = jet.Speed * particleMultiplier;
    }
}
