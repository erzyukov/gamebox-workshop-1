﻿using UnityEngine;

/// <summary>
/// Компонент главного меню
/// </summary>
public class MainMenu : MonoBehaviour
{
    /// <summary>
    /// Прятать кнопку выхода
    /// </summary>
    [SerializeField] private bool isHideQuitButton = default;

    /// <summary>
    /// Ссылка на объект кнопки выхода
    /// </summary>
    [SerializeField] private GameObject quitButton = default;

    private void Awake()
    {
        // убираем отображение кнопки выхода (для WebGL билда)
        if (quitButton && isHideQuitButton)
        {
            quitButton.SetActive(false);
        }
    }

    /// <summary>
    /// Обработка нажатия клавиши начала игры
    /// </summary>
    public void OnStartButtonPress()
    {
        //Загрузка следующей (первой) сцены
        GameManager.instance.scenes.LoadNextLevel();
    }

    /// <summary>
    /// Обработка нажатия клавиши выхода из игры
    /// </summary>
    public void OnQuitButtonPress()
    {
        GameManager.instance.scenes.QuitGame();
    }

}