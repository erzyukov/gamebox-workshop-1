﻿using System;
using UnityEngine;

/// <summary>
/// Компонент контроля прохождения круга состоящего из контрольных точек выбранным игроком
/// </summary>
public class PassingLapController : MonoBehaviour
{
    [Tooltip("Набор контрольных точек CheckPoint (в любом порядке)")]
    /// <summary>
    /// Набор контрольных точек CheckPoint
    /// </summary>
    [SerializeField] private CheckPoint[] points = default;

    /// <summary>
    /// номер предыдущей контрольной точки
    /// </summary>
    private int previousPointIndexNumber = default;
    /// <summary>
    /// тип предыдущей контрольной точки
    /// </summary>
    private CheckPoint.CheckPointType previousPointType;

    /// <summary>
    /// Номер текущего круга
    /// </summary>
    private int currentLapNumber = 0;
    /// <summary>
    /// Максимальный круг до которого дошел пользователь
    /// </summary>
    private int maxLapNumber = 0;
    /// <summary>
    /// Количество контрольных точек
    /// </summary>
    private int pointsCount = 0;

    /// <summary>
    /// Ссылка на компонент PlayerLapHandler
    /// </summary>
    private PlayerLapHandler playerLapHandler = null;

    private void Awake()
    {
        pointsCount = points.Length;
    }

    private void Start()
    {
        // подписываемся на события контрольных точек
        for (int i = 0; i < pointsCount; i++)
        {
            if (points[i] != null)
            {
                points[i].OnPointCross += OnPointCross;
            }
        }
    }

    public void InitLap(PlayerLapHandler handler)
    {
        playerLapHandler = handler;
    }

    /// <summary>
    /// Обработка пересечения игроком контрольной точки
    /// </summary>
    /// <param name="indexNumber">Номер контрольной точки</param>
    /// <param name="type">Тип контрольной точки</param>
    /// <param name="gameObjectID">Идентификатор объекта который пересек контрольную точку</param>
    private void OnPointCross(int indexNumber, CheckPoint.CheckPointType type, int gameObjectID)
    {
        if (playerLapHandler == null)
        {
            throw new Exception("Сначала необходимо инициализировать трассу с помощью функции InitLap!");
        }
        if (playerLapHandler.gameObject.GetInstanceID() == gameObjectID)
        {
            // если предыдущая точка была финишной, а текущая стартовая
            if (previousPointType == CheckPoint.CheckPointType.Finish && type == CheckPoint.CheckPointType.Start)
            {
                // если текущий круг меньше чем максимальный
                if (currentLapNumber < maxLapNumber)
                {
                    // значит пользователь проходил финишь в не верном направлении
                    // просто увеличиваем текущий круг
                    currentLapNumber++;
                }
                else
                {
                    // увеличиваем номер текущего круга
                    currentLapNumber++;
                    // увеличиваем номер круга до которого игрок доходил
                    maxLapNumber++;
                    // сообщаем пользователю о том, что он прошел круг
                    playerLapHandler.Increase();
                }
                // сообщаем пользователю о том, что он движется в верном направлении
                playerLapHandler.SetWrongWay(false);
            }

            // если прошли финишь в неверном направлении
            else if (previousPointType == CheckPoint.CheckPointType.Start && type == CheckPoint.CheckPointType.Finish)
            {
                // уменьшаем номер текущего круга
                currentLapNumber--;
                // сообщаем пользователю о том, что он движется в не верном направлении
                playerLapHandler.SetWrongWay(true);
            }

            // если номер текущей точки меньше номера предыдущей точки
            else if (indexNumber < previousPointIndexNumber)
            {
                // сообщаем пользователю о том, что он движется в не верном направлении
                playerLapHandler.SetWrongWay(true);
            }
            // если мы на предыдущем щаге шли не туда, а теперь движемся в верном направлении
            else if (playerLapHandler.IsWrongWay && indexNumber > previousPointIndexNumber)
            {
                // сообщаем пользователю о том, что он движется в верном направлении
                playerLapHandler.SetWrongWay(false);
            }

            // запоминаем данные пройденной точки
            previousPointIndexNumber = indexNumber;
            previousPointType = type;
        }
    }

    private void OnDestroy()
    {
        // отписываемся от событий контрольных точек
        for (int i = 0; i < pointsCount; i++)
        {
            if (points[i] != null)
            {
                points[i].OnPointCross -= OnPointCross;
            }
        }
    }

}
