﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Компонент пользовательского интерфейска отображающего активное усиление
/// </summary>
public class UIPlayerPowerUp : MonoBehaviour
{
    [Tooltip("Иконка усиления")]
    [SerializeField] private Image icon = null;
    [Tooltip("Текст с количеством усилений")]
    [SerializeField] private Text amount = null;

    [Tooltip("Номер игрока")]
    [SerializeField] private PlayerTypes type = default;

    private PowerUpContoller player;
    private int currentAmount = 0;

    private void Start()
    {
        player = GameManager.instance.resource.GetPlayer(type).GetComponent<PowerUpContoller>();

        if (player != null)
        {
            player.OnPickedUp += OnPickedUpPowerUp;
            player.OnUse += OnUsePowerUp;
        }

        ResetPowerUp();
    }

    /// <summary>
    /// Обработка поднятия усиления
    /// </summary>
    /// <param name="type"></param>
    /// <param name="maxAmount"></param>
    private void OnPickedUpPowerUp(PowerUpItem.Type type, int maxAmount)
    {
        icon.sprite = Resources.Load<Sprite>("UI/Icons/" + type);
        icon.enabled = true;
        amount.text = "x " + maxAmount;
        currentAmount = maxAmount;
        amount.enabled = true;
    }

    /// <summary>
    /// Обработка использования усиления
    /// </summary>
    private void OnUsePowerUp()
    {
        currentAmount--;
        if (currentAmount <= 0)
        {
            ResetPowerUp();
        }

        amount.text = "x " + currentAmount;
    }

    /// <summary>
    /// Сброс усиления
    /// </summary>
    private void ResetPowerUp()
    {
        icon.enabled = false;
        amount.enabled = false;
    }


    private void OnDestroy()
    {
        if (player != null)
        {
            player.OnPickedUp -= OnPickedUpPowerUp;
            player.OnUse -= OnUsePowerUp;
        }
    }
}
