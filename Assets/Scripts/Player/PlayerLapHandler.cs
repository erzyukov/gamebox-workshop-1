﻿using UnityEngine;

/// <summary>
/// Компонент хранящий и обрабатывающий информацию о прохождении круга игроком
/// </summary>
public class PlayerLapHandler : MonoBehaviour
{
    /// <summary>
    /// Номер текущего круга
    /// </summary>
    public byte LapNumber { get; private set; } = 0;
    /// <summary>
    /// Показатель неправильного направления движения
    /// </summary>
    public bool IsWrongWay { get { return isWrongWay; } }

    public delegate void LapPassedAction();
    /// <summary>
    /// Событие срабатывающее, когда игрок проходит круг
    /// </summary>
    public event LapPassedAction OnLapPassed;
    
    public delegate void WinnerAction(string name);
    /// <summary>
    /// Событие срабатывающее, когда игрок побеждает
    /// </summary>
    public event WinnerAction OnPlayerWin;

    public delegate void WrongWayAction();
    /// <summary>
    /// Событие срабатывающее, когда игрок движется в неверном направлении
    /// </summary>
    public event WrongWayAction OnWrongWay;
    /// <summary>
    /// Событие срабатывающее, когда игрок движется в верном направлении
    /// </summary>
    public event WrongWayAction OnRightWay;

    private bool isWrongWay = false;

    /// <summary>
    /// Устанавливает следующий номер круга
    /// </summary>
    public void Increase()
    {
        LapNumber++;

        // вызываем событие прохождения круга
        OnLapPassed?.Invoke();

        if (LapNumber > GameManager.instance.lapManager.MaxLapNumber)
        {
            OnPlayerWin?.Invoke(gameObject.name);
        }
    }

    /// <summary>
    /// Устанавливает показатель не правильного / правильного направления движения
    /// </summary>
    /// <param name="state">Состояние не верного направления</param>
    public void SetWrongWay(bool state)
    {
        isWrongWay = state;
        if (state)
        {
            // вызываем событие движение в неверном направлении
            OnWrongWay?.Invoke();
        }
        else
        {
            // вызываем событие движение в верном направлении
            OnRightWay?.Invoke();
        }
    }
    
}