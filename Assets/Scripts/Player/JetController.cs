﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Компонент управления двигателем катера
/// </summary>
[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class JetController : MonoBehaviour
{
    /// <summary>
    /// Максимальная скорость лодки для компонента Rigidbody
    /// </summary>
    public float MaxSpeed { get { return maxSpeed; } }

    public float Speed { 
        get {
            //return rb.velocity.magnitude;
            float value = (GameManager.instance == null || GameManager.instance.gameState.IsActionAllow())
                ? rb.velocity.magnitude
                : velocityMemory.magnitude;
            return (value > 0.1f) ? value : 0;
        } 
    }

    [Header("Настройки мощности движка")]
    
    [Tooltip("Мощность двигателя")]
    [SerializeField] private float jetPower = 1200;

    [Tooltip("Мощность крутящего момента")]
    [SerializeField] private float torquePower = 80;

    [Space]

    [Tooltip("Максимальная скорость")]
    [SerializeField] private float maxSpeed = 30;

    [Space]

    [Header("Настройки физики")]

    [Tooltip("Коэффициент скольжения")]
    [Range(0, 1)]
    [SerializeField] private float glideRate = .5f;

    [Tooltip("Масса лодки")]
    [SerializeField] private float boatMass = 50f;

    [Tooltip("Коэффициент скорости смены движения при повороте")]
    [Range(0, 1)]
    [SerializeField] private float rotationMoveRate = .2f;

    [Tooltip("Коэффициент торможения при повороте")]
    [SerializeField] private AnimationCurve rotationBreakRate = null;

    [Space]
    [Header("Ссылки на зависимые компоненты")]
    [SerializeField] private Rigidbody rb = null;
    [SerializeField] private Collider objectCollider = null;
    [SerializeField] private BreakdownController breakdownController = null;
    [SerializeField] private StatusController status = null;

    private Vector3 velocityMemory;
    
    private float defaultMaxSpeed;
    private Coroutine afterburnerCoroutine = null;

    private void Awake()
    {
        // инициализация значений стандартных компонент
        // устанавливается масса для компоненты Rigidbody
        rb.mass = boatMass;
        // устанавливается коэффициент скольжения для физического материала компоненты Collider
        objectCollider.material.dynamicFriction = glideRate;

        if (GameManager.instance != null && GameManager.instance.gameState != null)
        {
            GameManager.instance.gameState.OnGameStateChange += OnGameStateChange;
        }

        defaultMaxSpeed = maxSpeed;
    }

    private void Update()
    {
        if (GameManager.instance != null && !GameManager.instance.gameState.IsActionAllow())
        {
            return;
        }

        ProcessBoatDirection();
    }

    /// <summary>
    /// Меняет максимальную скорость
    /// </summary>
    /// <param name="amount">Значение</param>
    public void UpdateMaxSpeed(float amount)
    {
        if (afterburnerCoroutine != null)
        {
            StopCoroutine(afterburnerCoroutine);
        }
        maxSpeed = amount;
    }

    /// <summary>
    /// Сбрасывает максимальную скорость в значение по умолчанию
    /// </summary>
    public void ResetMaxSpeed()
    {
        if (afterburnerCoroutine != null)
        {
            StopCoroutine(afterburnerCoroutine);
        }
        maxSpeed = defaultMaxSpeed;
    }

    /// <summary>
    /// Добавляет мощности для двигателя
    /// </summary>
    public void AddPower()
    {
        // если есть компонент котнроля поломки и если лодка поломана - не позволяем давать мощности
        if (breakdownController != null && breakdownController.IsBroken())
        {
            return;
        }

        if (maxSpeed == 0)
        {
            rb.velocity = Vector3.zero;
        }
        // ограничиваем подачу силы, если превышена максимальная скорость
        else if (rb.velocity.magnitude < maxSpeed)
        {
            rb.AddRelativeForce(Vector3.forward * jetPower);
        }
        
    }

    /// <summary>
    /// Запускает форсаж
    /// </summary>
    /// <param name="duration">Длительность форсажа</param>
    /// <param name="maxSpeed">Максимальная скорость</param>
    public void RunAfterburner(float duration, float maxSpeed)
    {
        UpdateMaxSpeed(maxSpeed);
        afterburnerCoroutine = StartCoroutine(AfterburnerTimer(duration));
        if (status != null)
        {
            status.Set(StatusController.StatusTypes.Acceleration, duration);
        }

    }

    /// <summary>
    /// Добавляет лодке левый крутящий момент
    /// </summary>
    public void AddLeftTorque()
    {
        AddTorque(-1);
    }

    /// <summary>
    /// Добавляет лодке правый крутящий момент
    /// </summary>
    public void AddRightTorque()
    {
        AddTorque(1);
    }

    /// <summary>
    /// Добавляет лодке крутящий момент в зависимости от направления
    /// </summary>
    /// <param name="dir">Направление крутящего момента</param>
    private void AddTorque(sbyte dir)
    {
        // добавляем поворот в зависимости от выбранного направления поворота
        float yRotation = dir * Time.fixedDeltaTime * torquePower;
        if (yRotation != 0)
        {
            Quaternion currentLook = Quaternion.LookRotation(transform.forward, Vector3.up);
            currentLook *= Quaternion.Euler(0, yRotation, 0);
            transform.rotation = currentLook;
        }
    }

    /// <summary>
    /// Обрабатывает вектор движения лодки в зависимости от разницы 
    /// между вектором движения и вектором направления лодки
    /// </summary>
    private void ProcessBoatDirection()
    {
        if (rb.velocity.magnitude > 0.1f)
        {
            // угол между вектором движения и вектором направления лодки
            float angle = Vector3.Angle(rb.velocity, transform.forward);
            // если вектора движения и направления не совпадают

            if (angle > 0.1f)
            {
                // добавляем томожение в зависимости от поворота лодки (лодка идет боком)
                float breakRate = angle % 180 / 180;
                // торможение зависит от кривой, которая задается в настройках компоненты
                rb.velocity *= Mathf.Clamp01(rotationBreakRate.Evaluate(breakRate));

                // а так же усредняем направление между этими векторами,
                // чтобы лодка немного двигалась в сторону направления
                rb.velocity = Vector3.Lerp(rb.velocity, rb.velocity + transform.forward, Time.deltaTime * 10 * rotationMoveRate);
            }
        }
    }

    /// <summary>
    /// Обработка изменения состояния игры
    /// </summary>
    /// <param name="type"></param>
    private void OnGameStateChange(GameStateController.StateType type)
    {
        switch (type)
        {
            case GameStateController.StateType.Game:
                rb.velocity = velocityMemory;
                break;
            case GameStateController.StateType.Pause:
                velocityMemory = rb.velocity;
                rb.velocity = Vector3.zero;
                break;
        }
    }

    private IEnumerator AfterburnerTimer(float duration)
    {
        float timePassed = 0;
        while (timePassed < duration)
        {
            if (GameManager.instance.gameState.IsActionAllow())
            {
                timePassed += Time.fixedUnscaledDeltaTime;
            }
            yield return new WaitForFixedUpdate();
        }

        if (status != null)
        {
            status.Reset();
        }
        ResetMaxSpeed();
    }

    private void OnDestroy()
    {
        if (GameManager.instance != null && GameManager.instance.gameState != null)
        {
            GameManager.instance.gameState.OnGameStateChange -= OnGameStateChange;
        }
    }
}
