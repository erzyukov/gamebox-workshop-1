﻿using UnityEngine;

/// <summary>
/// Компонент отвечающий за контроллер игрока
/// </summary>
[RequireComponent(typeof(JetController))]
public class InputController : MonoBehaviour
{
    [Header("Настройки контроллера игрока")]
    /// <summary>
    /// Клавиша вперед
    /// </summary>
    [Tooltip("Клавиша вперед")]
    [SerializeField] private KeyCode forwardKey = default;
    /// <summary>
    /// Клавиша влево
    /// </summary>
    [Tooltip("Клавиша влево")]
    [SerializeField] private KeyCode leftKey = default;
    /// <summary>
    /// Клавиша вправо
    /// </summary>
    [Tooltip("Клавиша вправо")]
    [SerializeField] private KeyCode rightKey = default;

    /// <summary>
    /// Клавиша использования усиления
    /// </summary>
    [Tooltip("Клавиша использования усиления")]
    [SerializeField] private KeyCode powerUpKey = default;

    /// <summary>
    /// Ссылка на компонент управляющий двигателем катера
    /// </summary>
    [Tooltip("Ссылка на компонент игрока JetController")]
    [SerializeField] private JetController jet = null;

    /// <summary>
    /// Ссылка на компонент управляющий усилениями
    /// </summary>
    [Tooltip("Ссылка на компонент игрока JetController")]
    [SerializeField] private PowerUpContoller powerUp = null;

    /// <summary>
    /// Блокировка клавиши вперед
    /// </summary>
    public bool BlockForwardKey { get; set; }
    /// <summary>
    /// Блокировка клавиши усиления
    /// </summary>
    public bool BlockPowerUpKey { get; set; }


    private void FixedUpdate()
    {
        if (GameManager.instance != null && !GameManager.instance.gameState.IsActionAllow())
        {
            return;
        }

        // обрабатываем нажатие клавиши вперед
        if (!BlockForwardKey && Input.GetKey(forwardKey))
        {
            jet.AddPower();
        }

        // обрабатываем нажатие клавиши влево
        if (Input.GetKey(leftKey))
        {
            jet.AddLeftTorque();
        }
        // обрабатываем нажатие клавиши вправо
        else if (Input.GetKey(rightKey))
        {
            jet.AddRightTorque();
        }
    }

    private void Update()
    {
        if (GameManager.instance != null && !GameManager.instance.gameState.IsActionAllow())
        {
            return;
        }

        // обрабатываем нажакие клавиши использования усиления
        if (!BlockPowerUpKey && Input.GetKeyDown(powerUpKey))
        {
            powerUp.Use();
        }
    }

}
