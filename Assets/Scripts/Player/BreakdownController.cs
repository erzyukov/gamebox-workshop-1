﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Компонент контроля поломок
/// </summary>
public class BreakdownController : MonoBehaviour
{
    /// <summary>
    /// Ссылка на компонент JetController игрока
    /// </summary>
    [Tooltip("Ссылка на компонент JetController игрока")]
    [SerializeField] private JetController jet = null;

    /// <summary>
    /// Ссылка на компонент InputController игрока
    /// </summary>
    [Tooltip("Ссылка на компонент InputController игрока")]
    [SerializeField] private InputController input = null;

    /// <summary>
    /// Ссылка на компонент StatusController игрока
    /// </summary>
    [Tooltip("Ссылка на компонент StatusController игрока")]
    [SerializeField] private StatusController status = null;

    private bool isBroken = false;
    private Coroutine repearCoroutine;
    private float breakDuration;
    private float maxSpeed;

    /// <summary>
    /// Проверяем не поломана ли лодка. Если поломана - ломаем еще раз! }:-)
    /// </summary>
    /// <returns>Статус поломанности</returns>
    public bool IsBroken()
    {
        if (isBroken)
        {
            Break(breakDuration, maxSpeed);
        }
        return isBroken;
    }

    /// <summary>
    /// Повреждает игрока, понижая скорость на определенное время
    /// </summary>
    /// <param name="breakDuration">Длительность поломки</param>
    /// <param name="maxSpeed">Максимальная скорость при поломки</param>
    public void Break(float breakDuration, float maxSpeed)
    {
        this.breakDuration = breakDuration;
        this.maxSpeed = maxSpeed;
        if (repearCoroutine != null)
        {
            StopCoroutine(repearCoroutine);
        }
        jet.UpdateMaxSpeed(maxSpeed);
        input.BlockPowerUpKey = true;
        isBroken = true;
        if (status != null)
        {
            status.Set(StatusController.StatusTypes.Broken, breakDuration);
        }
        repearCoroutine = StartCoroutine(AutoRepearCoroutine(breakDuration));
    }

    /// <summary>
    /// Восстанавливает повреждения
    /// </summary>
    public void Repear()
    {
        isBroken = false;
        jet.ResetMaxSpeed();
        input.BlockPowerUpKey = false;
        if (status != null)
        {
            status.Reset();
        }
    }

    private IEnumerator AutoRepearCoroutine(float duration)
    {
        float timePassed = 0;
        while (timePassed < duration)
        {
            if (GameManager.instance.gameState.IsActionAllow())
            {
                timePassed += Time.fixedUnscaledDeltaTime;
            }
            yield return new WaitForFixedUpdate();
        }

        Repear();
    }

}
