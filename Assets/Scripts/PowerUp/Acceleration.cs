﻿using UnityEngine;

/// <summary>
/// Усиление. Повышает на определенное время максимальную скорость.
/// </summary>
public class Acceleration : IPowerUp
{

    public GameObject Target { get; set; }

    public PowerUpBehaviour Behaviour { get; set; }

    public bool Enabled { get; set; }

    private JetController targetJC;
    private float maxSpeed;
    private float duration;

    public Acceleration(float maxSpeed, float duration)
    {
        Behaviour = PowerUpBehaviour.Limited;
        Enabled = false;
        this.maxSpeed = maxSpeed;
        this.duration = duration;
    }

    public void PickUp(GameObject target)
    {
        Target = target;
        targetJC = target.GetComponent<JetController>();
    }

    public void Enable()
    {
        targetJC.RunAfterburner(duration, maxSpeed);
    }

    public void Disable()
    {
    }

}
