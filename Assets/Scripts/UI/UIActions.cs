﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Компонент для обработки взаимодействия пользователя с интерфейсом
/// </summary>
public class UIActions : MonoBehaviour
{
    [Tooltip("Кнопка запуска игры")]
    [SerializeField] private GameObject startButton = default;

    [Tooltip("Ссылка на компонет Text который выводит обратный отсчет")]
    /// <summary>
    /// Ссылка на компонет Text который выводит обратный отсчет
    /// </summary>
    [SerializeField] private Text countdownText = null;
    
    /// <summary>
    /// Обработка нажатия кнопки старт
    /// </summary>
    public void OnStartButtonPress()
    {
        startButton.SetActive(false);
        GameManager.instance.gameState.StartGame();
    }

    /// <summary>
    /// Обновляет значение поля для вывода обратного отчета
    /// </summary>
    /// <param name="value">Значение</param>
    public void UpdateCountdownText(string value)
    {
        countdownText.text = value;
    }

    /// <summary>
    /// Активирует объект отображающий обратный отсчет
    /// </summary>
    public void EnableCountdown()
    {
        countdownText.gameObject.SetActive(true);
    }

    /// <summary>
    /// Деактивирует объект отображающий обратный отсчет
    /// </summary>
    public void DisableCountdown()
    {
        countdownText.gameObject.SetActive(false);
    }

}
