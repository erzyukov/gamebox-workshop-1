﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Компонент для отсчета начала игры
/// </summary>
public class Countdown : MonoBehaviour
{
    /// <summary>
    /// Время до старта игры
    /// </summary>
    [SerializeField] private int timeToStart = 3;
    /// <summary>
    /// Текст, который выводится при старте, когда время равно 0
    /// </summary>
    [SerializeField] private string textForStart = "Начали!";

    private UnityAction StartGameAction;
    private Coroutine countdownCoroutine = null;

    /// <summary>
    /// Запускает отсчет
    /// </summary>
    public void StartCountdown(UnityAction action)
    {
        if (countdownCoroutine != null)
        {
            StopCoroutine(countdownCoroutine);
        }
        StartGameAction = action;
        GameManager.instance.resource.UIActions.EnableCountdown();
        countdownCoroutine = StartCoroutine(GameStart());
    }

    /// <summary>
    /// Метод для обратного отсчета
    /// После окончания отсчета запускается действие которое передавалось с методом StartCountdown
    /// </summary>
    private IEnumerator GameStart()
    {
        for (int i = timeToStart; i >= 0; i--)
        {
            if (i > 0)
            {
                GameManager.instance.resource.UIActions.UpdateCountdownText(i.ToString());
            }
            else if (i == 0)
            {
                GameManager.instance.resource.UIActions.UpdateCountdownText(textForStart);
            }

            yield return new WaitForSeconds(1f);
        }

        GameManager.instance.resource.UIActions.DisableCountdown();
        StartGameAction();
    }

}
