﻿using UnityEngine;

/// <summary>
/// Компонент управления окном паузы
/// </summary>
public class PauseWindow : Window
{
    private ConfirmWindow confirm = null;

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.gameState.IsPauseAllow())
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameManager.instance.gameState.State != GameStateController.StateType.Pause)
            {
                Open();
            }
            else
            {
                Close();
            }
        }
    }

    /// <summary>
    /// Устанавливает окно подтверждения
    /// </summary>
    /// <param name="window">Окно подтверждения</param>
    public void SetConfirmWindow(ConfirmWindow window)
    {
        confirm = window;
    }

    /// <summary>
    /// Открывает окно паузы и включает паузу
    /// </summary>
    public override void Open()
    {
        base.Open();
        GameManager.instance.gameState.PauseGame();
    }

    /// <summary>
    /// Закрывает окно паузы и выключает паузу
    /// </summary>
    public override void Close()
    {
        base.Close();
        GameManager.instance.gameState.ResumeGame();
    }

    /// <summary>
    /// Обработка нажатия кнопки главное меню
    /// </summary>
    public void MainMenuButtonHandler()
    {
        GameManager.instance.scenes.LoadMainMenu();
    }

    /// <summary>
    /// Обработка нажатия кнопки выход
    /// </summary>
    public void QuitButtonHandler()
    {
        confirm.Open();
        base.Close();
        confirm.SetYesAction(() => {
            GameManager.instance.scenes.QuitGame();
        });
        confirm.SetNoAction(() => {
            confirm.Close();
            base.Open();
        });
    }
}
